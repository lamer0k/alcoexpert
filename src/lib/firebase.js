import { db, auth } from '../config/firebase';
import { dateToString, calculateDate } from './source';

export const setProfileData = (userName, weight, gender, lang) => {
  const w = Number(weight);
  db.collection('profile').doc(auth.currentUser.uid).set({
    userName, weight: w, gender, lang,
  });
};

export const setJournalData = (date, amount, drinkId, drinkVol) => {
  db.collection('journal').add({
    uid: auth.currentUser.uid, date, amount, drinkId, drinkVol,
  });
};

export const getLastJournalRecords = (update, limit) => {
  db.collection('journal')
    .where('uid', '==', auth.currentUser.uid)
    .orderBy('date', 'desc')
    .limit(limit)
    .onSnapshot((snap) => {
      const data = [];
      snap.forEach((doc) => {
        // const formatedDate = secondToDate(doc.data().date.seconds);
        data.push({
          key: doc.id,
          date: dateToString(doc.data().date),
          amount: doc.data().amount,
          vol: doc.data().drinkVol,
          id: doc.data().drinkId,
        });
      });
      update(data);
    });
};

export const delJournalRecords = (key) => {
  db.collection('journal').doc(key).delete()
    .catch((error) => {
      console.error('Error removing document: ', error);
    });
};

export const getJournalDataDateFiltered = (update, days) => {
  const date = calculateDate(days);
  db.collection('journal')
    .where('uid', '==', auth.currentUser.uid)
    .where('date', '>', date)
    .orderBy('date')
    .limit(1000)
    .onSnapshot((snap) => {
      const data = [];
      snap.forEach((doc) => {
        // create date from timestamp
        // * 1000 to convert unix timestamp to date format
        // const formatedDate = secondToDate(doc.data().date.seconds);
        // create data arr
        data.push({
          date: dateToString(doc.data().date),
          amount: doc.data().amount,
          vol: doc.data().drinkVol,
          id: doc.data().drinkId,
        });
      });
      // set data to the state
      update(data);
    });
};

export const getFirstJournalDate = (update) => {
  db.collection('journal')
    .where('uid', '==', auth.currentUser.uid)
    .orderBy('date')
    .limit(1)
    .get()
    .then((snap) => {
      snap.forEach((doc) => {
        update(dateToString(doc.data().date));
      });
    });
};
