import React from 'react';
import { createStackNavigator, createBottomTabNavigator, createAppContainer } from 'react-navigation';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { colors } from './styles';
import Load from '../screens/Load';
import Login from '../screens/Login';
import Registration from '../screens/Registration';
import RegistrationDetails from '../screens/RegistrationDetails';
import CalculateDrinkList from '../screens/CalculateDrinkList';
import CalculateDrinkDetails from '../screens/CalculateDrinkDetails';
import Profile from '../screens/Profile';
import StatDrinkDetails from '../screens/StatDrinkDetails';
import StatDrinkLast from '../screens/StatDrinkLast';
import StatDrinkList from '../screens/StatDrinkList';
import StatIndex from '../screens/StatIndex';

import Test from '../screens/Test';


export const CalculateNav = createStackNavigator(
  {
    CalculateMain: CalculateDrinkList,
    CalculateDetails: CalculateDrinkDetails,
  },
  {
    initialRouteName: 'CalculateMain',
    headerMode: 'none',
  },
);

export const StatNav = createStackNavigator(
  {
    StatMain: StatIndex,
    StatLast: StatDrinkLast,
    StatAdd: StatDrinkList,
    StatAddDetail: StatDrinkDetails,
  },
  {
    initialRouteName: 'StatMain',
    headerMode: 'none',
  },
);

export const TabNavigator = createBottomTabNavigator(

  {
    Stat: {
      screen: StatNav,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => <Icon name="chart-bar" size={28} color={tintColor} />,
      },
    },
    Time: {
      screen: CalculateNav,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => <Icon name="calculator" size={28} color={tintColor} />,
      },
    },
    Profile: {
      screen: Profile,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => <Icon name="settings-outline" size={28} color={tintColor} />,
      },
    },
  },
  {
    tabBarOptions: {
      activeTintColor: colors.red,
      inactiveTintColor: colors.yellow,
      showLabel: false,
      style: {
        backgroundColor: colors.dark,
        borderTopWidth: 1,
        borderTopColor: colors.mediumDark,
      },
    },
  },
);

const MainNav = createStackNavigator(
  {
    Load,
    Login,
    Registration,
    RegistrationDetails,
    StatIndex: TabNavigator,
    // test: Test,
  },

  {
    // initialRouteName: 'Load',
    // initialRouteName: 'test',
    headerMode: 'none',
  },
);

const RootNav = createAppContainer(MainNav);

export default class MainNavigarion extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return <RootNav />;
  }
}
