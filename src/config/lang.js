import { getLanguages } from 'react-native-i18n';
import en from '../data/lang/en';
import ru from '../data/lang/ru';

let locale = 'en';
getLanguages().then((l) => {
  if (
    l[0] === 'ru'
    || l[0] === 'ru-MD'
    || l[0] === 'ru-RU'
    || l[0] === 'ru-UA'
    || l[0] === 'be'
    || l[0] === 'be-BY'
    || l[0] === 'uk'
    || l[0] === 'uk-UA') {
    locale = 'ru';
  }
});

const lang = locale === 'ru' ? ru : en;
export default lang;
