import React from 'react';
import { connect } from 'react-redux';
import { View } from 'react-native';
import styles from './styles';
import {
  calculatePromile,
  calculateVolMg,
  removalTime,
  ppmAdj,
  removalAdj,
  hoursToTime,
} from '../../lib/source';
import {
  SelectVol,
  TotalCircle,
  UpdateTotalVal,
} from '../../components/calculate';
import { ButtonNumberSet, ButtonSetText } from '../../components/buttons';
import { TimeModal } from '../../components/modals';
import { InnerTitle } from '../../components/texts';

class CalculatePromille extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      total: 0,
      vol: props.drink.val,
      modalVisible: false,
      eat: 0,
      activity: 0,
      ppm: 0,
      time: [0, 0],
    };
    this.updateVol = this.updateVol.bind(this);
    this.updateTotal = this.updateTotal.bind(this);
    this.updateEat = this.updateEat.bind(this);
    this.updateActivity = this.updateActivity.bind(this);
    this.updateModalVisible = this.updateModalVisible.bind(this);
    this.resetData = this.resetData.bind(this);
    this.calculate = this.calculate.bind(this);
  }

  updateVol(val) {
    this.setState({ vol: val });
  }

  updateTotal(val) {
    this.setState({ total: val });
  }

  updateEat(val) {
    this.setState({ eat: val });
  }

  updateActivity(val) {
    this.setState({ activity: val });
  }

  resetData() {
    const { drink } = this.props;
    this.setState({ total: 0, vol: drink.val });
  }

  updateModalVisible(val) {
    this.setState({ modalVisible: val });
  }

  calculate() {
    const { profile } = this.props;
    const {
      total,
      vol,
      activity,
      eat,
    } = this.state;
    const A = calculateVolMg(total, vol);
    const r = profile.gender === 'male' ? 0.7 : 0.6;
    const m = profile.weight;
    const ppmValAdj = ppmAdj(eat);
    const removalValAdj = removalAdj(profile.gender, activity);
    const ppmResult = calculatePromile(A, m, r, ppmValAdj);
    const hours = removalTime(ppmResult, removalValAdj);
    const timeResult = hoursToTime(hours);
    this.setState({
      modalVisible: true,
      ppm: ppmResult.toFixed(3),
      time: timeResult,
    });
  }

  render() {
    const { lang, drink } = this.props;
    const {
      total,
      vol,
      modalVisible,
      eat,
      activity,
      ppm,
      time,
    } = this.state;
    return (
      <View style={styles.detailWrap}>
        <TimeModal
          ppm={ppm}
          time={time}
          visible={modalVisible}
          update={this.updateModalVisible}
          lang={lang.calculate}
          hide={lang.hide}
          drink={drink.title}
          total={total}
        />
        <SelectVol title={drink.title.toUpperCase()} vol={vol} update={this.updateVol} />
        <TotalCircle totalText={lang.total} total={total} ml={lang.ml} />
        <UpdateTotalVal ml={lang.ml} total={total} update={this.updateTotal} />
        <InnerTitle title={lang.calculate.eatTitle} text={lang.calculate.eatText} />
        <ButtonNumberSet val={eat} update={this.updateEat} />
        <InnerTitle title={lang.calculate.moveTitle} text={lang.calculate.moveText} />
        <ButtonNumberSet val={activity} update={this.updateActivity} />
        <ButtonSetText
          update={this.resetData}
          updateTwo={this.calculate}
          textOne={lang.addAlcoDetail.reset.toUpperCase()}
          textTwo={lang.calculate.calc.toUpperCase()}
          iconOne="arrow-bottom-left-thick"
          iconTwo="arrow-top-right-thick"
          active={total}
        />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    // присваиваем ключам пропсов значения из хранилища редакса
    profile: state.profileData,
  };
}
// конектим стор и экшены с компонентом
export default connect(
  mapStateToProps,
)(CalculatePromille);
