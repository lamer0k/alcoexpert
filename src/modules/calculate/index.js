import CalculateMain from './CalculateMain';
import CalculatePromille from './CalculatePromille';

export {
  CalculateMain,
  CalculatePromille,
};
