import { StyleSheet } from 'react-native';
import { colors } from '../../config/styles';

export default StyleSheet.create({
  wrap: {
    marginHorizontal: 30,
  },
  detailWrap: {
    flex: 1,
    marginBottom: 40,
  },
  fieldTitleUnderlineWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 15,
    paddingVertical: 15,
    borderBottomWidth: 1,
    borderColor: colors.mediumDark,
  },
  fieldTitleYellow: {
    color: colors.yellow,
    fontSize: 20,
    marginTop: -3,
    marginLeft: 5,
  },
  fieldIcon: {
    color: colors.grey,
    marginRight: 5,
  },
  exit: {
    color: colors.red,
    marginLeft: 40,
  },
  succsess: {
    color: colors.yellow,
    fontWeight: '600',
  },
  error: {
    color: colors.red,
  },
});
