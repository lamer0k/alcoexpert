import React from 'react';
import { connect } from 'react-redux';
import { View } from 'react-native';
import styles from './styles';
import { FieldText } from '../../components/fields';
import { ListDrinks } from '../../components/lists';
import { getLanguageDrinks, searchListItem } from '../../lib/source';

class CalculateMain extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      search: '',
    };
    this.updateSearch = this.updateSearch.bind(this);
  }

  updateSearch(val) {
    this.setState({ search: val });
  }

  makeDrinkList() {
    const {
      profile,
      navigation,
      icon,
      page,
    } = this.props;
    const { search } = this.state;
    const data = getLanguageDrinks(profile.language);
    const filterDrinks = searchListItem(search, data);
    return (
      <ListDrinks data={filterDrinks} icon={icon} navigation={navigation} page={page} />
    );
  }

  render() {
    const { lang } = this.props;
    const { search } = this.state;
    return (
      <View style={styles.wrap}>
        <FieldText
          text={lang.addAlco.searchPlaceholder}
          val={search}
          update={this.updateSearch}
        />
        {this.makeDrinkList()}
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    // присваиваем ключам пропсов значения из хранилища редакса
    profile: state.profileData,
  };
}
// конектим стор и экшены с компонентом
export default connect(
  mapStateToProps,
)(CalculateMain);
