import LastAdded from './LastAdded';
import AveragePPM from './AveragePPM';
import TotalDrinks from './TotalDrinks';
import PopularDrinks from './PopularDrinks';

export {
  LastAdded,
  AveragePPM,
  TotalDrinks,
  PopularDrinks,
};
