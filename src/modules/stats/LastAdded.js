import React from 'react';
import { connect } from 'react-redux';
import {
  Text,
  View,
  Image,
  ActivityIndicator,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from './styles';
import { getLastJournalRecords, delJournalRecords } from '../../lib/firebase';
import { getLanguageDrinks } from '../../lib/source';
import { ButtonSetText } from '../../components/buttons';
// import {
//   dateToString,
// } from '../../source/source';


class LastAdded extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: '',
      activeItem: false,
    };
    this.updateActiveItem = this.updateActiveItem.bind(this);
    this.setActiveItem = this.setActiveItem.bind(this);
  }

  async componentDidMount() {
    const updateData = val => this.setState({ data: val });
    // set query data to the state
    // format [[date, amount, drinkVol, drinkId], ...]
    // last argument - period (days)
    await getLastJournalRecords(updateData, 15);
  }

  setActiveItem() {
    this.setState({ activeItem: '' });
  }

  updateActiveItem(val) {
    this.setState({ activeItem: val });
  }

  renderLoadView() {
    this.returnView = (
      <View style={styles.loadWrap}>
        <ActivityIndicator size="large" />
      </View>
    );
    return this.returnView;
  }

  renderDataView() {
    const { data, activeItem } = this.state;
    const { profile, lang } = this.props;
    const drinkData = getLanguageDrinks(profile.language);
    this.returnView = (
      <View>
        <FlatList
          data={data}
          keyExtractor={(item, index) => index.toString()}
          extraData={activeItem}
          renderItem={({ item, index }) => {
            const drinkList = drinkData.find(val => val.id === item.id);
            const delItem = () => {
              delJournalRecords(item.key);
              this.setState({ activeItem: false });
            };
            return (
              <View>
                <View style={styles.itemWrap}>
                  <View style={styles.itemTextWrapper}>
                    <Image
                      style={styles.itemImage}
                      source={drinkList.img}
                    />
                    <View>
                      <Text style={styles.itemDate}>{item.date}</Text>
                      <Text style={styles.itemTitle}>{drinkList.title}</Text>
                      <Text style={styles.itemText}>{`${item.amount}ml, ${item.vol}%`}</Text>
                    </View>
                  </View>
                  <TouchableOpacity onPress={() => this.updateActiveItem(index)}>
                    <Icon name="delete-outline" size={32} style={styles.itemIcon} />
                  </TouchableOpacity>
                </View>
                <View style={activeItem === index ? styles.delItem : styles.dNone}>
                  <ButtonSetText
                    update={this.setActiveItem}
                    updateTwo={delItem}
                    textOne={lang.cancel.toUpperCase()}
                    textTwo={lang.delite.toUpperCase()}
                    iconOne="arrow-bottom-left-thick"
                    iconTwo="arrow-top-right-thick"
                    active={item.key}
                  />
                </View>
              </View>
            );
          }}
        />
      </View>
    );
    return this.returnView;
  }

  render() {
    const { data } = this.state;
    if (data === '') return this.renderLoadView();
    return this.renderDataView();
  }
}

function mapStateToProps(state) {
  return {
    profile: state.profileData,
  };
}

// конектим стор и экшены с компонентом
export default connect(
  mapStateToProps,
)(LastAdded);
