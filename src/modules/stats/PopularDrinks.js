import React from 'react';
import { connect } from 'react-redux';
import {
  Text,
  View,
  ActivityIndicator,
  Image,
} from 'react-native';
import styles from './styles';
import {
  getLanguageDrinks,
} from '../../lib/source';


class PopularDrinks extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  renderLoadView() {
    this.returnView = (
      <View style={styles.loadWrap}>
        <ActivityIndicator size="large" />
      </View>
    );
    return this.returnView;
  }

  renderDataView() {
    const { lang, journal, profile } = this.props;

    const drinkData = getLanguageDrinks(profile.language);
    const arr = journal.records;
    const arr2 = [];
    arr.map((item) => {
      const result = arr2.findIndex(val => val[1] === item.id);
      if (result > -1) {
        arr2[result] = [arr2[result][0] += 1, arr2[result][1]];
      } else {
        arr2.push([1, item.id]);
      }
      return arr2.sort().reverse();
    });

    const drinkOne = arr2[0] ? drinkData.find(val => val.id === arr2[0][1]) : false;
    const drinkTwo = arr2[1] ? drinkData.find(val => val.id === arr2[1][1]) : false;
    const drinkThree = arr2[2] ? drinkData.find(val => val.id === arr2[2][1]) : false;

    const drinkView = (val, place) => {
      if (val !== false) {
        return (
          <View style={styles.itemWrap}>
            <View style={styles.itemTextWrapper}>
              <Image
                style={styles.itemImage}
                source={val.img}
              />
              <View>
                <Text style={styles.itemTitle}>{val.title}</Text>
                <Text style={styles.itemText}>{val.vol}</Text>
              </View>
            </View>
            <View>
              <Text style={styles.itemPlace}>{place}</Text>
            </View>
          </View>
        );
      }
      return (
        <View style={styles.itemWrap}>
          <View style={styles.itemTextWrapper}>
            <Text style={styles.itemTitle}>We collect data</Text>
          </View>
          <View>
            <Text style={styles.itemPlace}>{place}</Text>
          </View>
        </View>
      );
    };

    const returnView = (
      <View>
        <View style={styles.averWrap}>
          <Text style={styles.totalBlockTitle}>{lang.stat.popularTitle.toUpperCase()}</Text>
        </View>

        {drinkView(drinkOne, '№1')}
        {drinkView(drinkTwo, '№2')}
        {drinkView(drinkThree, '№3')}

      </View>
    );
    return returnView;
  }

  render() {
    const { journal } = this.props;
    if (!journal.records) return this.renderLoadView();
    return this.renderDataView();
  }
}

function mapStateToProps(state) {
  return {
    profile: state.profileData,
    journal: state.journalData,
  };
}
// конектим стор и экшены с компонентом
export default connect(
  mapStateToProps,
)(PopularDrinks);
