import { StyleSheet } from 'react-native';
import { colors } from '../../config/styles';

export default StyleSheet.create({
  redIcon: {
    color: colors.red,
    marginTop: 4,
  },
  greenIcon: {
    color: colors.green,
    marginTop: 4,
  },
  greyIcon: {
    color: colors.grey,
    marginTop: 4,
  },
  chartWrap: {
    marginHorizontal: 30,
    marginBottom: 40,
  },
  loadWrap: {
    marginVertical: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  itemWrap: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginHorizontal: 30,
    paddingVertical: 10,
    borderBottomWidth: 1,
    borderColor: colors.mediumDark,
  },
  itemTextWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  itemImage: {
    width: 65,
    height: 65,
    marginRight: 20,
    borderRadius: 50,
    borderWidth: 3,
    borderColor: colors.grey,
  },
  itemTitle: {
    color: colors.yellow,
    fontSize: 16,
    fontWeight: '600',
  },
  itemText: {
    color: colors.white,
  },
  itemDate: {
    color: colors.grey,
    fontSize: 12,
  },
  itemIcon: {
    color: colors.grey,
  },
  delItem: {
    backgroundColor: colors.mediumDark,
    paddingBottom: 30,
    marginBottom: 5,
  },
  dNone: {
    display: 'none',
  },
  averWrap: {
    paddingHorizontal: 30,
  },
  averWrapBlock: {
    backgroundColor: colors.mediumDark,
    padding: 20,
    marginTop: 20,
    borderRadius: 8,
  },
  averNumberWrap: {
    flexDirection: 'row',
    paddingTop: 5,
    marginTop: 5,
    justifyContent: 'space-between',
    borderTopWidth: 1,
    borderColor: colors.grey,
  },
  averNumberRed: {
    fontSize: 28,
    color: colors.red,
  },
  averNumberGreen: {
    fontSize: 28,
    color: colors.green,
  },
  averNumberWhite: {
    fontSize: 28,
    color: colors.white,
  },
  averPeriod: {
    color: colors.yellow,
  },
  averBlockTitle: {
    color: colors.white,
    fontWeight: '600',
    marginTop: 10,
  },
  averNumberTitle: {
    color: colors.grey,
    fontSize: 12,
  },
  averEqual: {
    color: colors.yellow,
    fontSize: 16,
  },
  infoText: {
    color: colors.grey,
    fontSize: 12,
  },
  totalBlockTitle: {
    color: colors.white,
    fontWeight: '600',
    marginTop: 30,
  },
  totalMgValue: {
    color: colors.red,
    fontSize: 28,
    paddingVertical: 7,
  },
  totalDrinkValue: {
    color: colors.grey,
    fontSize: 16,
    paddingTop: 7,
    borderBottomWidth: 1,
    borderColor: colors.mediumDark,
  },
  itemPlace: {
    color: colors.green,
    fontSize: 20,
    fontWeight: '600',
  },
});
