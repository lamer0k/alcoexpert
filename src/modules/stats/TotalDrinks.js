import React from 'react';
import { connect } from 'react-redux';
import {
  Text,
  View,
  ActivityIndicator,
} from 'react-native';
import styles from './styles';
import {
  calculateAlco,
} from '../../lib/source';


class TotalDrinks extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  renderLoadView() {
    this.returnView = (
      <View style={styles.loadWrap}>
        <ActivityIndicator size="large" />
      </View>
    );
    return this.returnView;
  }

  renderDataView() {
    const { lang, journal } = this.props;
    const v = calculateAlco(journal.records);
    // переводим алкоголь в спиртные напитки (объем \ % содержания \ мг -> мл \ мл -> л)
    const toBeer = (Number(v) / 0.036 / 0.79 / 1000).toFixed(2).toLocaleString('ru');
    const toVodka = (Number(v) / 0.4 / 0.79 / 1000).toFixed(2).toLocaleString('ru');
    // разделяем число на разряды
    const vWhisSpice = Number(v).toLocaleString('ru');
    const returnView = (
      <View style={styles.averWrap}>
        <Text style={styles.totalBlockTitle}>{lang.stat.totalTitle.toUpperCase()}</Text>
        <Text style={styles.totalMgValue}>{`${vWhisSpice} mg`}</Text>
        <Text style={styles.averEqual}>{`${lang.stat.equal}:`}</Text>
        <Text style={styles.totalDrinkValue}>{`${toBeer}${lang.stat.lBeer}`}</Text>
        <Text style={styles.totalDrinkValue}>{`${toVodka}${lang.stat.lVodka}`}</Text>
      </View>
    );
    return returnView;
  }

  render() {
    const { journal } = this.props;
    if (!journal.records) return this.renderLoadView();
    return this.renderDataView();
  }
}

function mapStateToProps(state) {
  return {
    profile: state.profileData,
    journal: state.journalData,
  };
}
// конектим стор и экшены с компонентом
export default connect(
  mapStateToProps,
)(TotalDrinks);
