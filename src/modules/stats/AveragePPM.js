import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
  Text,
  View,
  ActivityIndicator,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import styles from './styles';
import { getJournalDataDateFiltered, getFirstJournalDate } from '../../lib/firebase';
import { journalUpdateFirstDate, journalUpdateRecords } from '../../redux/journal';
import {
  averageAclo,
  limitOverrun,
  calculateDate,
  dateToString,
} from '../../lib/source';


class AveragePPM extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    const {
      updateFirstJournalDate,
      updateRecords,
    } = this.props;
    getJournalDataDateFiltered(updateRecords, -360);
    getFirstJournalDate(updateFirstJournalDate);
  }

  renderLoadView() {
    this.returnView = (
      <View style={styles.loadWrap}>
        <ActivityIndicator size="large" />
      </View>
    );
    return this.returnView;
  }

  renderWeekStats() {
    const { journal, lang } = this.props;
    // считаем в среднее употребление алкоголя за 7 дней
    const averageAcloMg = averageAclo(journal.records, 7, dateToString(calculateDate(-6)));
    // считаем в среднее употребление алкоголя за 14 дней
    const averageAcloMg2 = averageAclo(journal.records, 14, dateToString(calculateDate(-13)));
    // считаем отклонение от рекомендуемой нормы
    const norm = limitOverrun(averageAcloMg);
    const toBeer = (Number(averageAcloMg) / 0.036 / 0.79 / 1000).toFixed(2).toLocaleString('ru');
    let trend = (
      <Icon
        style={Number(averageAcloMg) > Number(averageAcloMg2) ? styles.redIcon : styles.greenIcon}
        name={Number(averageAcloMg) > Number(averageAcloMg2) ? 'dislike1' : 'like1'}
        size={28}
      />
    );

    let text = <Text style={styles.dNone} />;
    // если дата первой в журнал внесена до 2х недельного срока
    // мы не считаем тренд
    if (!journal.firstJournalDate || journal.firstJournalDate > dateToString(calculateDate(-13))) {
      trend = <Icon name="questioncircleo" size={28} style={styles.greyIcon} />;
      text = <Text style={styles.infoText}>{lang.stat.infoAverageText}</Text>;
    }
    return (
      <View style={styles.averWrapBlock}>
        <Text style={styles.averPeriod}>{lang.stat.lastWeek}</Text>
        <View style={styles.averNumberWrap}>
          <View>
            <Text style={styles.averNumberTitle}>{lang.mg}</Text>
            <Text style={styles.averNumberWhite}>{averageAcloMg}</Text>
          </View>
          <View>
            <Text style={styles.averNumberTitle}>{lang.stat.norm}</Text>
            <Text style={norm > 0 ? styles.averNumberRed : styles.averNumberGreen}>{norm}</Text>
          </View>
          <View>
            <Text style={styles.averNumberTitle}>{lang.stat.trend}</Text>
            {trend}
          </View>
        </View>
        <Text style={styles.averEqual}>{`${lang.stat.equal}: ${toBeer}${lang.stat.lBeer}`}</Text>
        {text}
      </View>
    );
  }

  renderMonthStats() {
    const { journal, lang } = this.props;
    // считаем в среднее употребление алкоголя за 30 дней
    const averageAcloMg = averageAclo(journal.records, 30, dateToString(calculateDate(-29)));
    // считаем в среднее употребление алкоголя за 60 дней
    const averageAcloMg2 = averageAclo(journal.records, 60, dateToString(calculateDate(-59)));
    // считаем отклонение от рекомендуемой нормы
    const norm = limitOverrun(averageAcloMg);
    const toBeer = (Number(averageAcloMg) / 0.036 / 0.79 / 1000).toFixed(2).toLocaleString('ru');
    let trend = (
      <Icon
        style={Number(averageAcloMg) > Number(averageAcloMg2) ? styles.redIcon : styles.greenIcon}
        name={Number(averageAcloMg) > Number(averageAcloMg2) ? 'dislike1' : 'like1'}
        size={28}
      />
    );
    let text = <Text style={styles.dNone} />;
    // если дата первой в журнал внесена до 2х недельного срока
    // мы не считаем тренд
    if (!journal.firstJournalDate || journal.firstJournalDate > dateToString(calculateDate(-59))) {
      trend = <Icon name="questioncircleo" size={28} style={styles.greyIcon} />;
      text = <Text style={styles.infoText}>{lang.stat.infoAverageText}</Text>;
    }
    return (
      <View style={styles.averWrapBlock}>
        <Text style={styles.averPeriod}>{lang.stat.lastMonth}</Text>
        <View style={styles.averNumberWrap}>
          <View>
            <Text style={styles.averNumberTitle}>{lang.mg}</Text>
            <Text style={styles.averNumberWhite}>{averageAcloMg}</Text>
          </View>
          <View>
            <Text style={styles.averNumberTitle}>{lang.stat.norm}</Text>
            <Text style={norm > 0 ? styles.averNumberRed : styles.averNumberGreen}>{norm}</Text>
          </View>
          <View>
            <Text style={styles.averNumberTitle}>{lang.stat.trend}</Text>
            {trend}
          </View>
        </View>
        <Text style={styles.averEqual}>{`${lang.stat.equal}: ${toBeer}${lang.stat.lBeer}`}</Text>
        {text}
      </View>
    );
  }

  renderDataView() {
    const { lang } = this.props;
    const returnView = (
      <View style={styles.averWrap}>
        <Text style={styles.averBlockTitle}>{lang.stat.averageTitle.toUpperCase()}</Text>
        {this.renderWeekStats()}
        {this.renderMonthStats()}
      </View>

    );
    return returnView;
  }

  render() {
    const { journal } = this.props;
    if (!journal.records) return this.renderLoadView();
    return this.renderDataView();
  }
}

function mapStateToProps(state) {
  return {
    profile: state.profileData,
    journal: state.journalData,
  };
}
function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    updateFirstJournalDate: journalUpdateFirstDate,
    updateRecords: journalUpdateRecords,
  }, dispatch);
}
// конектим стор и экшены с компонентом
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AveragePPM);
