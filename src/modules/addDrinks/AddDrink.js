import React from 'react';
import { connect } from 'react-redux';
import { View } from 'react-native';
import styles from './styles';
import { calculateDate } from '../../lib/source';
import { setJournalData } from '../../lib/firebase';
import {
  SelectVol,
  TotalCircle,
  UpdateTotalVal,
  UpdateDate,
} from '../../components/calculate';
import { ButtonSetText } from '../../components/buttons';
import { SuccessModal } from '../../components/modals';

class AddDrink extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      total: 0,
      changeDate: 0,
      vol: props.drink.val,
      modalVisible: false,
    };
    this.updateVol = this.updateVol.bind(this);
    this.updateTotal = this.updateTotal.bind(this);
    this.updateDate = this.updateDate.bind(this);
    this.resetData = this.resetData.bind(this);
    this.setData = this.setData.bind(this);
    this.updateModalVisible = this.updateModalVisible.bind(this);
  }

  setData() {
    const { changeDate, total, vol } = this.state;
    const { drink } = this.props;
    setJournalData(
      calculateDate(changeDate),
      total,
      drink.id,
      vol,
    );
    this.setState({
      total: 0,
      changeDate: 0,
      vol: drink.val,
      modalVisible: true,
    });
  }

  updateVol(val) {
    this.setState({ vol: val });
  }

  updateTotal(val) {
    this.setState({ total: val });
  }

  updateDate(val) {
    this.setState({ changeDate: val });
  }

  resetData() {
    const { drink } = this.props;
    this.setState({ total: 0, vol: drink.val });
  }

  updateModalVisible(val) {
    this.setState({ modalVisible: val });
  }

  render() {
    const { lang, drink, navigation } = this.props;
    const {
      total,
      vol,
      modalVisible,
      changeDate,
    } = this.state;
    return (
      <View style={styles.detailWrap}>

        <SuccessModal
          visible={modalVisible}
          update={this.updateModalVisible}
          navigation={navigation}
          moveTo="StatLast"
          moveToBtn={lang.addAlcoDetail.toStats}
          back={lang.addAlcoDetail.addMore}
          title={lang.addAlcoDetail.modalTitle}
          hide={lang.hide}
        />

        <SelectVol title={drink.title.toUpperCase()} vol={vol} update={this.updateVol} />
        <TotalCircle totalText={lang.total} total={total} ml={lang.ml} />
        <UpdateTotalVal ml={lang.ml} total={total} update={this.updateTotal} />
        <UpdateDate
          val={changeDate}
          text={lang.addAlcoDetail.selectDate}
          update={this.updateDate}
        />
        <ButtonSetText
          update={this.resetData}
          updateTwo={this.setData}
          textOne={lang.addAlcoDetail.reset.toUpperCase()}
          textTwo={lang.addAlcoDetail.addDrink.toUpperCase()}
          iconOne="arrow-bottom-left-thick"
          iconTwo="arrow-top-right-thick"
          active={total}
        />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    // присваиваем ключам пропсов значения из хранилища редакса
    profile: state.profileData,
  };
}
// конектим стор и экшены с компонентом
export default connect(
  mapStateToProps,
)(AddDrink);
