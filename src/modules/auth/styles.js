import { StyleSheet } from 'react-native';
import { colors } from '../../config/styles';

export default StyleSheet.create({
  wrap: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  flexWrap: {
    backgroundColor: colors.dark,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logoImage: {
    width: 96,
    height: 96,
    marginTop: -100,
  },
  titleText: {
    fontSize: 24,
    fontWeight: '600',
    color: colors.red,
    marginBottom: 20,
  },
  textInput: {
    backgroundColor: colors.mediumDark,
    width: 260,
    paddingHorizontal: 20,
    paddingVertical: 7,
    marginBottom: 10,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: colors.deepGrey,
    color: colors.white,
  },
  authText: {
    color: colors.white,
  },
  authTextBold: {
    color: colors.yellow,
    fontWeight: '600',
    paddingHorizontal: 8,
  },
  authWrap: {
    marginTop: 20,
    flexDirection: 'row',
  },
  error: {
    color: colors.yellow,
    marginBottom: 5,
  },
});
