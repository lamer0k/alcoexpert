import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getLanguages } from 'react-native-i18n';
import { View, Text } from 'react-native';
import lang from '../../config/lang';
import { setProfileData } from '../../lib/firebase';
import styles from './styles';
import { ButtonBigRedSet, ButtonTextArrow } from '../../components/buttons';
import { FieldNumberBig } from '../../components/fields';
import { WhiteTitleCenter } from '../../components/texts';


class SignUpDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      step: 0,
      gender: 'male',
      weight: '',
      language: 'en',
      error: '',
    };
    this.updateStep = this.updateStep.bind(this);
    this.updateGender = this.updateGender.bind(this);
    this.updateWeight = this.updateWeight.bind(this);
    this.registrationEnd = this.registrationEnd.bind(this);
  }

  componentDidMount() {
    getLanguages().then(l => this.setState({ language: l[0] }));
  }

  updateStep(val) {
    this.setState({ step: val });
  }

  updateGender(val) {
    this.setState({ gender: val });
  }

  updateWeight(val) {
    this.setState({ weight: val });
  }

  registrationEnd() {
    const { gender, weight, language } = this.state;
    if (weight > 20) {
      setProfileData('User', weight, gender, language);
    } else {
      this.setState({ error: lang.profile.errorWeight });
    }
  }

  renderStepOne() {
    const { step, gender } = this.state;
    return (
      <View style={styles.flexWrap}>
        <WhiteTitleCenter
          titleWhite={lang.profile.selectYour.toUpperCase()}
          titleRed={lang.profile.gender.toUpperCase()}
          text={lang.profile.necessaryCalc}
        />
        <ButtonBigRedSet
          gender={{ male: lang.profile.male, female: lang.profile.female }}
          icon={{ one: 'gender-male', two: 'gender-female' }}
          active={gender}
          update={this.updateGender}
        />
        <ButtonTextArrow
          icon="arrow-top-right-thick"
          color="yellow"
          text={lang.confirm.toUpperCase()}
          val={step + 1}
          update={this.updateStep}
        />
      </View>
    );
  }

  renderStepTwo() {
    const { error, weight } = this.state;
    return (
      <View style={styles.flexWrap}>
        <WhiteTitleCenter
          titleWhite={lang.profile.selectYour.toUpperCase()}
          titleRed={lang.profile.weight.toUpperCase()}
          text={lang.profile.necessaryCalc}
        />
        <FieldNumberBig
          val={weight}
          update={this.updateWeight}
          text="0"
        />
        <Text style={styles.error}>{error}</Text>
        <ButtonTextArrow
          icon="arrow-top-right-thick"
          color="yellow"
          text={lang.confirm.toUpperCase()}
          update={this.registrationEnd}
        />
      </View>
    );
  }

  render() {
    const { step } = this.state;
    if (step === 0) return this.renderStepOne();
    return this.renderStepTwo();
  }
}

function mapStateToProps(state) {
  return {

  };
}
// передаем в пропсы экшены и диспачим их
function mapDispatchToProps(dispatch) {
  return bindActionCreators({

  }, dispatch);
}
// конектим стор и экшены с компонентом
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SignUpDetails);
