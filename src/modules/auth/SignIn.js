import React from 'react';
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import { auth } from '../../config/firebase';
import lang from '../../config/lang';
import styles from './styles';
import { colors } from '../../config/styles';
import { ButtonBigRed } from '../../components/buttons';

const imgLogoIcon = require('../../images/icon.png');

class SignIn extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      error: '',
    };
  }

  render() {
    const { navigation } = this.props;
    const { email, password, error } = this.state;

    return (
      <View style={styles.wrap}>
        <Image
          style={styles.logoImage}
          source={imgLogoIcon}
        />
        <Text style={styles.titleText}>
          {lang.login.title.toUpperCase()}
        </Text>
        <Text style={styles.error}>{error}</Text>
        <TextInput
          style={styles.textInput}
          keyboardType="email-address"
          autoCapitalize="none"
          placeholderTextColor={colors.grey}
          onChangeText={mail => this.setState({ email: mail })}
          value={email}
          placeholder={lang.emial}
        />
        <TextInput
          style={styles.textInput}
          placeholderTextColor={colors.grey}
          secureTextEntry
          onChangeText={pass => this.setState({ password: pass })}
          value={password}
          placeholder={lang.password}
        />
        <TouchableOpacity onPress={() => {
          auth.signInWithEmailAndPassword(email, password)
            .catch((alert) => {
              const errorMessage = alert.message;
              this.setState({ error: errorMessage });
            });
        }}
        >
          <ButtonBigRed text={lang.login.signIn} />
        </TouchableOpacity>
        <View style={styles.authWrap}>
          <Text style={styles.authText}>
            {lang.login.regText}
          </Text>
          <Text style={styles.authTextBold} onPress={() => navigation.navigate('Registration')}>
            {lang.login.regLink.toUpperCase()}
          </Text>
        </View>
      </View>
    );
  }
}

export default SignIn;
