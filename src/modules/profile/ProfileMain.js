import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {
  Text,
  View,
} from 'react-native';
import { auth } from '../../config/firebase';
import { getLanguage } from '../../lib/source';
import styles from './styles';
import { setProfileData } from '../../lib/firebase';
import { ButtonSetTwoGrey, ButtonMaxRed } from '../../components/buttons';
import { profileUpdate } from '../../redux/profile';
import { FieldTitle } from '../../components/texts';
import { FieldNumber } from '../../components/fields';

class ProfileMain extends React.Component {
  constructor(props) {
    super(props);
    const { profileData } = this.props;
    this.state = {
      gender: profileData.gender,
      weight: profileData.weight,
      language: profileData.language,
      successMessage: '',
      weightError: '',
    };
    this.updateWeight = this.updateWeight.bind(this);
    this.updateGender = this.updateGender.bind(this);
    this.updateLang = this.updateLang.bind(this);
    this.setChanges = this.setChanges.bind(this);
  }

  setChanges() {
    const { gender, weight, language } = this.state;
    const { updateProfile, profileData } = this.props;
    const lang = getLanguage(profileData.language);
    if (weight > 20) {
      setProfileData(
        'User',
        weight,
        gender,
        language,
      );
      updateProfile({ language, gender, weight });
      this.setState({
        successMessage: lang.profile.successMessage,
        weightError: '',
      });
    } else {
      this.setState({ weightError: lang.profile.emptyWeightError });
    }
  }

  updateWeight(val) {
    this.setState({ weight: val });
  }

  updateGender(val) {
    this.setState({ gender: val });
  }

  updateLang(val) {
    this.setState({ language: val });
  }

  render() {
    const { profileData } = this.props;
    const lang = getLanguage(profileData.language);
    const {
      weightError,
      weight,
      gender,
      language,
      successMessage,
    } = this.state;
    let changeLang = 'en';
    if (
      language === 'ru'
      || language === 'ru-RU'
      || language === 'ru-MD'
      || language === 'ru-UA'
    ) {
      changeLang = 'ru';
    }
    return (
      <View style={styles.wrap}>
        <View style={styles.fieldTitleUnderlineWrapper}>
          <Icon name="email-outline" size={22} style={styles.fieldIcon} />
          <Text style={styles.fieldTitleYellow}>{auth.currentUser.email}</Text>
          <Text style={styles.exit} onPress={() => auth.signOut()}>
            {lang.profile.exit}
          </Text>
        </View>
        <Text style={styles.error}>{weightError}</Text>
        <FieldTitle text={lang.profile.weight} icon="weight" />
        <FieldNumber
          text={lang.enterData}
          val={weight}
          update={this.updateWeight}
        />
        <FieldTitle text={lang.profile.gender} icon="gender-male-female" />
        <ButtonSetTwoGrey
          oneText={lang.profile.male}
          oneVal="male"
          twoText={lang.profile.female}
          twoVal="female"
          active={gender}
          update={this.updateGender}
        />
        <FieldTitle text={lang.profile.lang} icon="book-open" />
        <ButtonSetTwoGrey
          oneText={lang.profile.en}
          oneVal="en"
          twoText={lang.profile.ru}
          twoVal="ru"
          active={changeLang}
          update={this.updateLang}
        />
        <Text style={styles.succsess}>{successMessage}</Text>
        <ButtonMaxRed text={lang.save} action={this.setChanges} />
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    // присваиваем ключам пропсов значения из хранилища редакса
    profileData: state.profileData,
  };
}
// передаем в пропсы экшены и диспачим их
function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    updateProfile: profileUpdate,
  }, dispatch);
}
// конектим стор и экшены с компонентом
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ProfileMain);
