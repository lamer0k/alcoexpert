const initialState = {};

export const profileUpdate = data => ({
  type: 'PROFILE_UPDATE',
  payload: data,
});

export default function profileData(state = initialState, action) {
  switch (action.type) {
    case 'PROFILE_UPDATE':
      return {
        ...state,
        language: action.payload.lang,
        gender: action.payload.gender,
        weight: action.payload.weight,
      };
    default:
      return state;
  }
}
