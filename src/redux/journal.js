const initialState = {};

export const journalUpdateFirstDate = data => ({
  type: 'JOURNAL_UPDATE_FIRST_DATE',
  payload: data,
});

export const journalUpdateRecords = data => ({
  type: 'JOURNAL_UPDATE_RECORDS',
  payload: data,
});

export default function journalData(state = initialState, action) {
  switch (action.type) {
    case 'JOURNAL_UPDATE_FIRST_DATE':
      return {
        ...state,
        firstJournalDate: action.payload,
      };
    case 'JOURNAL_UPDATE_RECORDS':
      return {
        ...state,
        records: action.payload,
      };
    default:
      return state;
  }
}
