import { combineReducers } from 'redux';
import journalData from './journal';
import profileData from './profile';

export default combineReducers({
  journalData,
  profileData,
});
