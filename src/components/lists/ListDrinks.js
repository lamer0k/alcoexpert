import React from 'react';
import {
  Text,
  View,
  FlatList,
  TouchableOpacity,
  Image,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from './styles';


const ListDrinks = (props) => {
  const {
    data,
    icon,
    navigation,
    page,
  } = props;
  return (
    <FlatList
      data={data}
      keyExtractor={(item, index) => index.toString()}
      renderItem={({ item }) => (
        <View style={styles.itemWrapper}>
          <View style={styles.itemTextWrapper}>
            <Image
              style={styles.itemImage}
              source={item.img}
            />
            <View>
              <Text style={styles.itemTitle}>{item.title}</Text>
              <Text style={styles.itemText}>{item.vol}</Text>
            </View>
          </View>
          <TouchableOpacity onPress={() => navigation.navigate(page, item)}>
            <Icon name={icon} size={32} style={styles.itemIcon} />
          </TouchableOpacity>
        </View>
      )
      }
    />
  );
};
export default ListDrinks;
