import { StyleSheet } from 'react-native';
import { colors } from '../../config/styles';

export default StyleSheet.create({
  itemWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 10,
    borderBottomWidth: 1,
    borderColor: colors.mediumDark,
  },
  itemTextWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  itemImage: {
    width: 65,
    height: 65,
    marginRight: 20,
    borderRadius: 50,
    borderWidth: 3,
    borderColor: colors.grey,
  },
  itemTitle: {
    color: colors.yellow,
    fontSize: 16,
    fontWeight: '600',
  },
  itemText: {
    color: colors.white,
  },
  itemIcon: {
    color: colors.grey,
  },
});
