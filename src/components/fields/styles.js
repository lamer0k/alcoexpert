import { StyleSheet } from 'react-native';
import { colors } from '../../config/styles';

export default StyleSheet.create({
  numberBig: {
    backgroundColor: colors.mediumDark,
    borderColor: colors.deepGrey,
    borderWidth: 2,
    borderRadius: 10,
    width: 120,
    height: 120,
    fontSize: 32,
    fontWeight: '600',
    textAlign: 'center',
    color: colors.white,
    marginBottom: 30,
  },
  textInput: {
    backgroundColor: colors.mediumDark,
    width: '100%',
    paddingHorizontal: 20,
    paddingVertical: 7,
    marginBottom: 20,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: colors.deepGrey,
    color: colors.white,
  },
});
