import React from 'react';
import { TextInput } from 'react-native';
import styles from './styles';
import { colors } from '../../config/styles';

const FieldText = (props) => {
  const {
    text,
    val,
    update,
  } = props;

  return (
    <TextInput
      style={styles.textInput}
      maxLength={30}
      onChangeText={value => update(value)}
      value={`${val}`}
      placeholder={text}
      placeholderTextColor={colors.grey}
    />
  );
};
export default FieldText;
