import React from 'react';
import { TextInput } from 'react-native';
import styles from './styles';
import { colors } from '../../config/styles';

const FieldNumber = (props) => {
  const {
    text,
    val,
    update,
  } = props;

  return (
    <TextInput
      style={styles.textInput}
      keyboardType="number-pad"
      maxLength={3}
      onChangeText={value => update(value)}
      value={`${val}`}
      placeholder={text}
      placeholderTextColor={colors.grey}
    />
  );
};
export default FieldNumber;
