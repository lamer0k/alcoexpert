import React from 'react';
import { Text, View } from 'react-native';
import styles from './styles';

const ButtonNumberSet = (props) => {
  const { val, update } = props;
  const btnStyleOne = val === 0 ? styles.numberSetTextActive : styles.numberSetText;
  const btnStyleTwo = val === 1 ? styles.numberSetTextActive : styles.numberSetText;
  const btnStyleThree = val === 2 ? styles.numberSetTextActive : styles.numberSetText;
  return (
    <View style={styles.numberSetWrap}>
      <Text
        style={btnStyleOne}
        onPress={() => update(0)}
      >
        1
      </Text>
      <Text
        style={btnStyleTwo}
        onPress={() => update(1)}
      >
        2
      </Text>
      <Text
        style={btnStyleThree}
        onPress={() => update(2)}
      >
        3
      </Text>
    </View>
  );
};
export default ButtonNumberSet;
