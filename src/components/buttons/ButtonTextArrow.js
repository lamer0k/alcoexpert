import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from './styles';
import { colors } from '../../config/styles';

const ButtonTextArrow = (props) => {
  const {
    text,
    icon,
    color,
    val,
    update,
  } = props;
  let iconStyle = colors.red;
  let textStyle = styles.textArrowTextRed;

  if (color === 'yellow') {
    iconStyle = colors.yellow;
    textStyle = styles.textArrowTextYellow;
  } else if (color === 'grey') {
    iconStyle = colors.grey;
    textStyle = styles.textArrowTextGrey;
  }
  return (
    <TouchableOpacity style={styles.textArrowWrap} onPress={() => update(val)}>
      <Icon
        name={icon}
        color={iconStyle}
        size={28}
      />
      <Text style={textStyle}>{text}</Text>
    </TouchableOpacity>
  );
};
export default ButtonTextArrow;
