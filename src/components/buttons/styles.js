import { StyleSheet } from 'react-native';
import { colors } from '../../config/styles';

export default StyleSheet.create({
  yellow: {
    color: colors.yellow,
  },
  grey: {
    color: colors.grey,
  },
  red: {
    color: colors.red,
  },
  bigRedSetWrap: {
    width: '100%',
    marginHorizontal: 30,
    marginBottom: 30,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  bigRedButton: {
    backgroundColor: colors.red,
    width: 120,
    height: 120,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bigGreyButton: {
    backgroundColor: colors.mediumDark,
    width: 120,
    height: 120,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bigRedWrapper: {
    backgroundColor: colors.red,
    width: 260,
    height: 45,
    marginTop: 3,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
  },
  maxWidthRedWrapper: {
    backgroundColor: colors.red,
    width: '100%',
    height: 45,
    marginTop: 15,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
  },
  bigRedText: {
    color: colors.white,
  },
  textArrowWrap: {
    flexDirection: 'row',
  },
  textArrowTextYellow: {
    color: colors.yellow,
    fontWeight: '600',
    fontSize: 18,
    marginLeft: 3,
    marginTop: 2,
  },
  textArrowTextRed: {
    color: colors.red,
    fontWeight: '600',
    fontSize: 18,
    marginLeft: 3,
    marginTop: 2,
  },
  textArrowTextGrey: {
    color: colors.grey,
    fontWeight: '600',
    fontSize: 18,
    marginLeft: 3,
    marginTop: 2,
  },
  setTwoGreyWrap: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  setTwoGreyText: {
    backgroundColor: colors.mediumDark,
    width: '46%',
    textAlign: 'center',
    paddingHorizontal: 20,
    paddingTop: 13,
    paddingBottom: 9,
    marginBottom: 10,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: colors.deepGrey,
    color: colors.grey,
  },
  setTwoGreyTextActive: {
    backgroundColor: colors.mediumDark,
    width: '46%',
    textAlign: 'center',
    paddingHorizontal: 20,
    paddingTop: 13,
    paddingBottom: 9,
    marginBottom: 10,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: colors.yellow,
    color: colors.yellow,
  },
  numberSetWrap: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
    marginHorizontal: 30,
  },
  numberSetText: {
    backgroundColor: colors.mediumDark,
    width: '30%',
    textAlign: 'center',
    paddingHorizontal: 20,
    paddingTop: 13,
    paddingBottom: 9,
    marginBottom: 10,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: colors.deepGrey,
    color: colors.grey,
  },
  numberSetTextActive: {
    backgroundColor: colors.mediumDark,
    width: '30%',
    textAlign: 'center',
    paddingHorizontal: 20,
    paddingTop: 13,
    paddingBottom: 9,
    marginBottom: 10,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: colors.yellow,
    color: colors.yellow,
  },
  buttonSetTextWrap: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginHorizontal: 30,
    marginTop: 30,
  },
  setTextWrap: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  setTextYellow: {
    color: colors.yellow,
    fontSize: 18,
    fontWeight: '600',
  },
  setTextRed: {
    color: colors.red,
    fontSize: 18,
    fontWeight: '600',
  },
  setTextGrey: {
    color: colors.grey,
    fontSize: 18,
    fontWeight: '600',
  },
});
