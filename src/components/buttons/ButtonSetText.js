import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from './styles';

const ButtonSetText = (props) => {
  const {
    update,
    updateTwo,
    textOne,
    textTwo,
    iconOne,
    iconTwo,
    active,
  } = props;

  const btnStyle = active === 0 ? styles.setTextGrey : styles.setTextRed;
  const btnIconStyle = active === 0 ? styles.grey : styles.red;

  return (
    <View style={styles.buttonSetTextWrap}>
      <TouchableOpacity
        style={styles.setTextWrap}
        onPress={() => update()}
      >
        <Icon name={iconOne} size={24} style={styles.yellow} />
        <Text style={styles.setTextYellow}>
          {textOne}
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.setTextWrap}
        onPress={() => {
          if (active !== 0) updateTwo();
        }}
      >
        <Icon name={iconTwo} size={24} style={btnIconStyle} />
        <Text style={btnStyle}>
          {textTwo}
        </Text>
      </TouchableOpacity>
    </View>
  );
};
export default ButtonSetText;
