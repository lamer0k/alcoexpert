import React from 'react';
import { Text, View } from 'react-native';
import styles from './styles';

const ButtonSetTwoGrey = (props) => {
  const {
    oneText,
    oneVal,
    twoText,
    twoVal,
    active,
    update,
  } = props;
  const oneStyle = oneVal === active ? styles.setTwoGreyTextActive : styles.setTwoGreyText;
  const twoStyle = twoVal === active ? styles.setTwoGreyTextActive : styles.setTwoGreyText;
  return (
    <View style={styles.setTwoGreyWrap}>
      <Text
        style={oneStyle}
        onPress={() => update(oneVal)}
      >
        {oneText}
      </Text>
      <Text
        style={twoStyle}
        onPress={() => update(twoVal)}
      >
        {twoText}
      </Text>
    </View>
  );
};
export default ButtonSetTwoGrey;
