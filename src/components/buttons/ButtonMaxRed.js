import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import styles from './styles';

const ButtonMaxRed = (props) => {
  const { text, action } = props;
  return (
    <TouchableOpacity style={styles.maxWidthRedWrapper} onPress={() => action()}>
      <Text style={styles.bigRedText}>{text}</Text>
    </TouchableOpacity>
  );
};
export default ButtonMaxRed;
