import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { colors } from '../../config/styles';
import styles from './styles';

const ButtonBigRedSet = (props) => {
  const {
    gender,
    icon,
    active,
    update,
  } = props;
  const oneStyle = active === 'male' ? styles.bigRedButton : styles.bigGreyButton;
  const twoStyle = active === 'female' ? styles.bigRedButton : styles.bigGreyButton;
  return (
    <View style={styles.bigRedSetWrap}>
      <TouchableOpacity style={oneStyle} onPress={() => update('male')}>
        <Icon
          name={icon.one}
          color={colors.white}
          size={32}
        />
        <Text style={styles.bigRedText}>{gender.male}</Text>
      </TouchableOpacity>
      <TouchableOpacity style={twoStyle} onPress={() => update('female')}>
        <Icon
          name={icon.two}
          color={colors.white}
          size={32}
        />
        <Text style={styles.bigRedText}>{gender.female}</Text>
      </TouchableOpacity>
    </View>
  );
};
export default ButtonBigRedSet;
