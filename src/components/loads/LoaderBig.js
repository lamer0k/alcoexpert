import React from 'react';
import { View, ActivityIndicator } from 'react-native';
import styles from './styles';

const LoaderBig = () => (
  <View style={styles.bigWrap}>
    <ActivityIndicator size="large" />
  </View>
);
export default LoaderBig;
