import React from 'react';
import { View, ActivityIndicator } from 'react-native';
import styles from './styles';

const LoaderSmall = () => (
  <View style={styles.smallWrap}>
    <ActivityIndicator size="large" />
  </View>
);
export default LoaderSmall;
