import { StyleSheet } from 'react-native';
import { colors } from '../../config/styles';

export default StyleSheet.create({
  smallWrap: {
    paddingVertical: 30,
    alignItems: 'center',
  },
  bigWrap: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
