import LoaderBig from './LoaderBig';
import LoaderSmall from './LoaderSmall';

export {
  LoaderBig,
  LoaderSmall,
};
