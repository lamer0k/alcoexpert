import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from './styles';

const HeaderBack = (props) => {
  const { navigation } = props;
  return (
    <View style={styles.headerBackWrapper}>
      <TouchableOpacity onPress={() => navigation.goBack()}>
        <Icon name="arrow-left" size={32} style={styles.headerBackIcon} />
      </TouchableOpacity>
    </View>
  );
};

export default HeaderBack;
