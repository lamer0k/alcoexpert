import { StyleSheet } from 'react-native';
import { colors } from '../../config/styles';

export default StyleSheet.create({
  headerBackWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 25,
    marginBottom: -10,
    marginHorizontal: 30,
  },
  headerBackIcon: {
    color: colors.red,
    marginTop: 10,
  },
});
