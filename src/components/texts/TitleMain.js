import React from 'react';
import { Text, View } from 'react-native';
import styles from './styles';

const TitleMain = (props) => {
  const { titleWhite, titleRed, text } = props;
  return (
    <View style={styles.headerWrap}>
      <View style={styles.titleWrap}>
        <Text style={styles.whiteTitleCenter}>{titleWhite}</Text>
        <Text style={styles.whiteTitleCenterRed}>{titleRed}</Text>
      </View>
      <Text style={styles.whiteTitleCenterText}>{text}</Text>
    </View>
  );
};
export default TitleMain;
