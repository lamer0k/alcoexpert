import { StyleSheet } from 'react-native';
import { colors } from '../../config/styles';

export default StyleSheet.create({
  wrap: {
    width: '100%',
    paddingHorizontal: 30,
    alignItems: 'center',
  },
  titleWrap: {
    flexDirection: 'row',
  },
  whiteTitleCenter: {
    color: colors.white,
    fontSize: 18,
    fontWeight: '600',
  },
  whiteTitleCenterRed: {
    color: colors.red,
    fontSize: 18,
    fontWeight: '600',
    marginLeft: 7,
  },
  whiteTitleCenterText: {
    color: colors.grey,
    textAlign: 'center',
    fontSize: 16,
    marginBottom: 30,
  },
  headerWrap: {
    width: '100%',
    paddingHorizontal: 30,
    alignItems: 'flex-start',
    marginTop: 40,
  },
  fieldIcon: {
    color: colors.grey,
    marginRight: 5,
  },
  fieldTitleWrapper: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    marginVertical: 4,
  },
  fieldTitle: {
    color: colors.grey,
    fontSize: 16,
  },
  titleInnerWrap: {
    marginHorizontal: 30,
    marginBottom: 10,
  },
  titleInnerHeader: {
    fontSize: 18,
    fontWeight: '600',
    marginTop: 10,
    color: colors.yellow,
  },
  titleInnerText: {
    color: colors.white,
  },
});
