import WhiteTitleCenter from './whiteTitleCenter';
import TitleMain from './TitleMain';
import FieldTitle from './FieldTitle';
import InnerTitle from './InnerTitle';

export {
  WhiteTitleCenter,
  TitleMain,
  FieldTitle,
  InnerTitle,
};
