import React from 'react';
import { Text, View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from './styles';


const FieldTitle = (props) => {
  const { text, icon } = props;
  return (
    <View style={styles.fieldTitleWrapper}>
      <Icon name={icon} size={22} style={styles.fieldIcon} />
      <Text style={styles.fieldTitle}>{text}</Text>
    </View>
  );
};
export default FieldTitle;
