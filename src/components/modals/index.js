import SuccessModal from './SuccessModal';
import TimeModal from './TimeModal';

export {
  SuccessModal,
  TimeModal,
};
