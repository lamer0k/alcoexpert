import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { View } from 'react-native';
import { auth, db } from '../config/firebase';
import { profileUpdate } from '../redux/profile';
import styles from './styles';
import { LoaderBig } from '../components/loads';

class Load extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    const { navigation, updateProfile } = this.props;
    // auth.signOut()
    auth.onAuthStateChanged((user) => {
      // если пользователь не активирован перенаправляем на регистрацию
      if (user === null) {
        navigation.navigate('Registration');
      } else {
        // select user profile from DB.
        db.collection('profile').doc(auth.currentUser.uid)
          .onSnapshot((doc) => {
            if (!doc.data()) {
              navigation.navigate('RegistrationDetails');
            } else {
              // if profile exist - update store
              updateProfile(doc.data());
              // if profile exist - redirect to main app page
              navigation.navigate('StatIndex');
            }
          });
      }
    });
  }

  render() {
    return (
      <View style={styles.wrap}>
        <LoaderBig />
      </View>
    );
  }
}
function mapStateToProps(state) {
  return {
    profile: state.profileData,
  };
}
// передаем в пропсы экшены и диспачим их
function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    updateProfile: profileUpdate,
  }, dispatch);
}
// конектим стор и экшены с компонентом
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Load);
