import React from 'react';
import { connect } from 'react-redux';
import { View, ScrollView } from 'react-native';
import { getLanguage } from '../lib/source';
import styles from './styles';
import { LastAdded } from '../modules/stats';
import { TitleMain } from '../components/texts';
import { ThreeTabNav } from '../components/navigate';

class StatDrinkList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { profile, navigation } = this.props;
    const lang = getLanguage(profile.language);
    return (
      <View style={styles.wrap}>
        <ScrollView>
          <TitleMain
            titleWhite={lang.stat.title.toUpperCase()}
            titleRed={lang.stat.titleRed.toUpperCase()}
            text={lang.stat.headerText}
          />
          <ThreeTabNav
            navigation={navigation}
            linkOne="StatMain"
            linkTwo="StatAdd"
            linkThree="StatLast"
            textOne={lang.stat.stats.toUpperCase()}
            textTwo={lang.stat.addDrink.toUpperCase()}
            textThree={lang.stat.lastDrinks.toUpperCase()}
            active={3}
          />
          <LastAdded lang={lang} />
        </ScrollView>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    profile: state.profileData,
  };
}

// конектим стор и экшены с компонентом
export default connect(
  mapStateToProps,
)(StatDrinkList);
