import React from 'react';
import { View } from 'react-native';
import styles from './styles';
import { SignUp } from '../modules/auth';


class Registration extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { navigation } = this.props;
    return (
      <View style={styles.wrap}>
        <SignUp navigation={navigation} />
      </View>
    );
  }
}

export default Registration;
