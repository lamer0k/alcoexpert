import { StyleSheet } from 'react-native';
import { colors } from '../config/styles';

export default StyleSheet.create({
  wrap: {
    backgroundColor: colors.dark,
    minHeight: '100%',
  },
  flexWrap: {
    backgroundColor: colors.dark,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
