import React from 'react';
import { View, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import styles from './styles';
import { getLanguage } from '../lib/source';
import { ProfileMain } from '../modules/profile';
import { TitleMain } from '../components/texts';

class Profile extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { profileData } = this.props;
    const lang = getLanguage(profileData.language);
    return (
      <View style={styles.wrap}>
        <ScrollView>
          <TitleMain
            titleWhite={lang.profile.title.toUpperCase()}
            titleRed={lang.profile.titleRed.toUpperCase()}
            text={lang.profile.headerText}
          />
          <ProfileMain />
        </ScrollView>
      </View>
    );
  }
}
function mapStateToProps(state) {
  return {
    // присваиваем ключам пропсов значения из хранилища редакса
    profileData: state.profileData,
  };
}
// конектим стор и экшены с компонентом
export default connect(
  mapStateToProps,
)(Profile);
