import React from 'react';
import { View } from 'react-native';
import styles from './styles';
import { SignIn } from '../modules/auth';


class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { navigation } = this.props;
    return (
      <View style={styles.wrap}>
        <SignIn navigation={navigation} />
      </View>
    );
  }
}

export default Login;
