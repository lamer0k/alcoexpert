import React from 'react';
import { connect } from 'react-redux';
import { View, ScrollView } from 'react-native';
import { getLanguage } from '../lib/source';
import { TitleMain } from '../components/texts';
import { HeaderBack } from '../components/headers';
import { CalculatePromille } from '../modules/calculate';
import styles from './styles';

class CalculateDrinkDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      drink: props.navigation.state.params,
    };
  }

  render() {
    const { profile, navigation } = this.props;
    const lang = getLanguage(profile.language);
    const { drink } = this.state;
    return (
      <View style={styles.wrap}>
        <ScrollView>
          <HeaderBack navigation={navigation} />
          <TitleMain
            titleWhite={lang.calculate.title.toUpperCase()}
            titleRed={lang.calculate.titleRed.toUpperCase()}
            text={lang.addAlcoDetail.headerText}
          />
          <CalculatePromille navigation={navigation} drink={drink} lang={lang} />
        </ScrollView>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    profile: state.profileData,
  };
}

// конектим стор и экшены с компонентом
export default connect(
  mapStateToProps,
)(CalculateDrinkDetails);
