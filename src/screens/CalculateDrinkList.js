import React from 'react';
import { connect } from 'react-redux';
import { View, ScrollView } from 'react-native';
import { getLanguage } from '../lib/source';
import styles from './styles';
import { TitleMain } from '../components/texts';
import { CalculateMain } from '../modules/calculate';

class CalculateDrinkList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { profile, navigation } = this.props;
    const lang = getLanguage(profile.language);
    return (
      <View style={styles.wrap}>
        <ScrollView>
          <TitleMain
            titleWhite={lang.calculate.title.toUpperCase()}
            titleRed={lang.calculate.titleRed.toUpperCase()}
            text={lang.calculate.headerText}
          />
          <CalculateMain lang={lang} navigation={navigation} icon="arrow-right" page="CalculateDetails" />
        </ScrollView>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    profile: state.profileData,
  };
}

// конектим стор и экшены с компонентом
export default connect(
  mapStateToProps,
)(CalculateDrinkList);
