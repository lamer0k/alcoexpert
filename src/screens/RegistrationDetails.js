import React from 'react';
import { View } from 'react-native';
import styles from './styles';
import { SignUpDetails } from '../modules/auth';


class ProfileFirstData extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={styles.flexWrap}>
        <SignUpDetails />
      </View>
    );
  }
}

export default ProfileFirstData;
