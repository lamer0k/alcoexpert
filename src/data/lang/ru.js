const ru = {
  emial: 'eMail',
  password: 'пароль',
  save: 'Сохранить',
  enterData: 'Введите данные',
  total: 'Всего',
  ml: 'мл',
  mg: 'Мг',
  hide: 'Закрыть',
  confirm: 'Подтвердить',
  delite: 'Удалить',
  cancel: 'Отмена',
  registration: {
    title: 'Регистрация',
    signUp: 'Регистрация',
    authText: 'Уже есть аккаунт?',
    authLink: 'Вход',
  },
  login: {
    title: 'Вход',
    signIn: 'Вход',
    regText: 'нет аккаунта?',
    regLink: 'Регистрация',
  },
  addAlco: {
    title: 'Добавить',
    titleRed: 'Напиток',
    headerText: 'выберите напиток, который вы пили',
    searchPlaceholder: 'Введите название напитка:',
  },
  addAlcoDetail: {
    title: 'Укажите',
    titleRed: 'Детали',
    headerText: 'выберите дату и объем напитка',
    selectDate: 'Выберите дату:',
    successMessage: 'данные успешно внесены',
    addDrink: 'Добавить',
    reset: 'Сбросить',
    toStats: 'Cтатистика',
    addMore: 'Добавить',
    modalTitle: 'Напиток добавлен',
  },
  calculate: {
    title: 'Расчет',
    titleRed: 'Дозировки',
    headerText: 'выберите напиток, который вы пили',
    calc: 'Рассчитать',
    eatTitle: 'Сколько вы ели?',
    eatText: 'Выберите от 1 до 3, где 1 - мало',
    moveTitle: 'Как много вы двигались?',
    moveText: 'Выберите от 1 до 3, где 1 - мало',
    promille: 'Промилле',
    ppmModalText: 'Концентрация алкоголя в крови. Максимальная концентрация достигается в течении 1-2 часов после употребления напитка.',
    timeModalTitle: 'Время очищения',
    timeModalText: 'Cреднее время очищения организма от алкоголя',
    timeAttention: 'Внимание!!! Время выведения алкоголя зависит от индивидуальных особенностей организма и может отличаться от данного прогноза.',
    alcoStady: {
      a: 'Трезвость',
      b: 'Легкое опьянение',
      c: 'Среднее опьянение',
      d: 'Сильное опьянение',
      e: 'Алкогольное отравление',
      f: 'Возможно кома',
      j: 'Возможна смерть',
    },
  },
  profile: {
    selectYour: 'Выберите ваш',
    necessaryCalc: 'Необходимо для подсчета статистики',
    emptyWeightError: 'Укажите ваш вес',
    successMessage: 'Данные успешно сохранены',
    title: 'Настройки',
    titleRed: 'Аккаунта',
    exit: 'Выход',
    headerText: 'данные доступны только вам',
    userName: 'Имя',
    gender: 'Пол',
    male: 'Мужской',
    female: 'Женский',
    weight: 'Вес (кг)',
    lang: 'Язык',
    en: 'Англ.',
    ru: 'Рус.',
    startMessage: 'Для подолжения работы укажите недостающие данные.',
    errorWeight: 'Введите корректный вес',
  },
  stat: {
    title: 'Твоя',
    titleRed: 'Статистика',
    headerText: 'держи все под контролем',
    stats: 'Данные',
    addDrink: 'Добавить',
    lastDrinks: 'Последнее',
    lastWeek: 'Среднее за 7 дней',
    lastMonth: 'Среднее за 30 дней',
    norm: 'Превышение нормы',
    trend: 'Тренд',
    infoAverageText: 'Мы собираем данные. Продолжайте отмечать выпитые напитки для получения более точной статистики.',
    averageTitle: 'Среднее дневное употребление чистого спирта',
    totalTitle: 'Общее употребление чистого спирта',
    equal: 'Это равно',
    lBeer: 'л Пива',
    lVodka: 'л Водки',
    popularTitle: 'Популярные напитки',
  },
};

export default ru;
