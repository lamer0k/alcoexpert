import React from 'react';
import { YellowBox } from 'react-native';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import reduser from './src/redux';
import MainNavigarion from './src/config/routes';

YellowBox.ignoreWarnings(['Setting a timer', 'Require cycle:']);

const store = createStore(reduser);

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Provider store={store}>
        <MainNavigarion />
      </Provider>
    );
  }
}
